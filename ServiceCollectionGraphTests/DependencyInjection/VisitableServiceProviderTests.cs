﻿namespace ServiceCollectionGraphTests.DependencyInjection;

using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using ServiceCollectionGraph.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using static ServiceCollectionGraphTests.DependencyInjection.DependencyVisitorFake;
using static ServiceCollectionGraphTests.TestServices;

public class VisitableServiceProviderTests
{
    [Fact]
    public void GetService_SameScope_CreatesObjects()
    {
        var sc = new ServiceCollection();
        sc.AddSingleton<ILeaf, Leaf>();
        sc.AddTransient<ILeafUser, LeafUser>();
        sc.AddScoped<ILeafUserUser, LeafUserUser>();
        sc.AddTransient<ITwiceUser, TwiceUser>();

        using var sp = new VisitableServiceProvider(sc);
        var a = sp.GetService<ITwiceUser>();
        var b = sp.GetService<ITwiceUser>();

        a?.LeafUserUser.Should().BeSameAs(b?.LeafUserUser);
        new[]
        {
            a?.LeafUserUser?.LeafUser,
            a?.LeafUser,
            b?.LeafUser
        }.ToHashSet().Should().HaveCount(3).And.NotContainNulls();
        new[]
        {
            a?.LeafUserUser?.LeafUser?.Leaf,
            a?.LeafUser?.Leaf,
            b?.LeafUser?.Leaf
        }.ToHashSet().Should().HaveCount(1).And.NotContainNulls();
    }

    [Fact]
    public void GetService_DifferentScope_CreatesObjects()
    {
        var sc = new ServiceCollection();
        sc.AddSingleton<ILeaf, Leaf>();
        sc.AddTransient<ILeafUser, LeafUser>();
        sc.AddScoped<ILeafUserUser, LeafUserUser>();
        sc.AddTransient<ITwiceUser, TwiceUser>();

        using var sp = new VisitableServiceProvider(sc);
        using var s0 = sp.CreateScope();
        using var s1 = sp.CreateScope();
        var a = s0.ServiceProvider.GetService<ITwiceUser>();
        var b = s1.ServiceProvider.GetService<ITwiceUser>();

        new[]
        {
            a?.LeafUserUser?.LeafUser,
            b?.LeafUserUser?.LeafUser,
            a?.LeafUser,
            b?.LeafUser
        }.ToHashSet().Should().HaveCount(4).And.NotContainNulls();
        new[]
        {
            a?.LeafUserUser?.LeafUser?.Leaf,
            b?.LeafUserUser?.LeafUser?.Leaf,
            a?.LeafUser?.Leaf,
            b?.LeafUser?.Leaf
        }.ToHashSet().Should().HaveCount(1).And.NotContainNulls();
    }

    [Fact]
    public void GetService_MultipleServices_UsesLastAdded()
    {
        var sc = new ServiceCollection();
        sc.AddTransient<ILeaf, Leaf>();
        sc.AddSingleton<ILeaf, Leaf>();

        using var sp = new VisitableServiceProvider(sc);
        var a = sp.GetService<ILeaf>();
        var b = sp.GetService<ILeaf>();

        a.Should().BeSameAs(b);
        a.Should().NotBeNull();
    }

    [Fact]
    public void GetService_GenericType_UsesCorrectType()
    {
        var sc = new ServiceCollection();
        sc.AddSingleton(typeof(IGeneric<>), typeof(Generic<>));
        using var sp = new VisitableServiceProvider(sc);

        var a = sp.GetService<IGeneric<int>>();

        a.Should().BeOfType<Generic<int>>();
    }

    [Fact]
    public void GetService_IEnumerable_ReturnsRegisteredServicesInOrder0()
    {
        var sc = new ServiceCollection();
        var instance = new Leaf();
        sc.AddSingleton<ILeaf, Leaf>(o => instance);
        sc.AddTransient<ILeaf, Leaf>();
        using var sp = new VisitableServiceProvider(sc);

        var a = sp.GetService<IEnumerable<ILeaf>>()?.ToList();

        a.Should().HaveCount(2);
        a![0].Should().BeSameAs(instance);
    }

    [Fact]
    public void GetService_IEnumerable_ReturnsRegisteredServicesInOrder1()
    {
        var sc = new ServiceCollection();
        var instance = new Leaf();
        sc.AddTransient<ILeaf, Leaf>();
        sc.AddSingleton<ILeaf, Leaf>(o => instance);
        using var sp = new VisitableServiceProvider(sc);

        var a = sp.GetService<IEnumerable<ILeaf>>()?.ToList();

        a.Should().HaveCount(2);
        a![1].Should().BeSameAs(instance);
    }

    [Fact]
    public void GetService_IEnumerableWithGenerics_UsesOrderOfAddition0()
    {
        var sc = new ServiceCollection();
        var instance = new Generic<int>();
        sc.AddTransient<IGeneric<int>>(o => instance);
        sc.AddSingleton(typeof(IGeneric<>), typeof(Generic<>));
        using var sp = new VisitableServiceProvider(sc);

        var a = sp.GetService<IEnumerable<IGeneric<int>>>()?.ToList();

        a.Should().HaveCount(2);
        a![0].Should().BeSameAs(instance);
    }

    [Fact]
    public void GetService_IEnumerableWithGenerics_UsesOrderOfAddition1()
    {
        var sc = new ServiceCollection();
        var instance = new Generic<int>();
        sc.AddSingleton(typeof(IGeneric<>), typeof(Generic<>));
        sc.AddTransient<IGeneric<int>>(o => instance);
        using var sp = new VisitableServiceProvider(sc);

        var a = sp.GetService<IEnumerable<IGeneric<int>>>()?.ToList();

        a.Should().HaveCount(2);
        a![1].Should().BeSameAs(instance);
    }

    [Fact]
    public void GetService_AddIEnumerableToServiceCollection_OverridesDefaultBehavior()
    {
        var sc = new ServiceCollection();
        var firstInstance = new Leaf();
        var secondInstance = new Leaf();
        var thirdInstance = new Leaf();
        sc.AddTransient<ILeaf>(o => firstInstance);
        sc.AddTransient<IEnumerable<ILeaf>>(o => new ILeaf[] { secondInstance });
        sc.AddTransient<ILeaf>(o => thirdInstance);
        using var sp = new VisitableServiceProvider(sc);

        var a = sp.GetService<IEnumerable<ILeaf>>();

        a.Should().Equal(secondInstance);
    }

    [Fact]
    public void GetService_CreateServiceProvider_ReturnsSelf()
    {
        var sc = new ServiceCollection();
        using var sp = new VisitableServiceProvider(sc);

        var a = sp.GetService<IServiceProvider>();

        a.Should().BeSameAs(sp);
    }

    [Fact]
    public void GetService_CreateServiceScopeFactory_ReturnsSelf()
    {
        var sc = new ServiceCollection();
        using var sp = new VisitableServiceProvider(sc);

        var a = sp.GetService<IServiceScopeFactory>();

        a.Should().BeSameAs(sp);
    }

    [Fact]
    public void GetService_ServiceIsNull_ThrowsArgumentNullException()
    {
        var sc = new ServiceCollection();
        using var sp = new VisitableServiceProvider(sc);

        var getService = () => sp.GetService(null!);

        getService.Should().Throw<ArgumentNullException>();
    }

    [Fact]
    public void GetService_WithExampleServices_CallsVisitor()
    {
        var sc = new ServiceCollection();
        sc.AddSingleton<ILeaf, Leaf>();
        sc.AddTransient<ILeafUser, LeafUser>();
        sc.AddScoped<ILeafUserUser, LeafUserUser>();
        sc.AddTransient<ITwiceUser, TwiceUser>();
        var visitorFake = new DependencyVisitorFake();
        using var sp = new VisitableServiceProvider(sc, visitorFake);

        var a = sp.GetService<ITwiceUser>();
        var b = sp.GetService<ITwiceUser>();

        visitorFake.Calls.Should().Equal(
            Call.Begin(typeof(ITwiceUser)),
              Call.Begin(typeof(ILeafUser)),
                Call.Begin(typeof(ILeaf)),
                Call.End(a!.LeafUser.Leaf),
              Call.End(a.LeafUser),
              Call.Begin(typeof(ILeafUserUser)),
                Call.Begin(typeof(ILeafUser)),
                  Call.Begin(typeof(ILeaf)),
                  Call.End(a.LeafUserUser.LeafUser.Leaf),
                Call.End(a.LeafUserUser.LeafUser),
              Call.End(a.LeafUserUser),
            Call.End(a),
            Call.Begin(typeof(ITwiceUser)),
              Call.Begin(typeof(ILeafUser)),
                Call.Begin(typeof(ILeaf)),
                Call.End(b!.LeafUser.Leaf),
              Call.End(b.LeafUser),
              Call.Begin(typeof(ILeafUserUser)),
              // ILeafUserUser is scoped and has been instantiated for `a`.
              // Therefore, its dependencies do not need to be resolved here.
              Call.End(b.LeafUserUser),
            Call.End(b));
    }

    [Fact]
    public void DisposeServiceProviderAndScope_DisposesServicesAtCorrectPlace()
    {
        var sc = new ServiceCollection();
        sc.AddScoped<IDisposeTracker<Tag0>, DisposeTracker<Tag0>>();
        sc.AddTransient<IDisposeTracker<Tag1>>(o => new DisposeTracker<Tag1>(o.GetService<IDisposeTracker<Tag0>>()));
        sc.AddSingleton<IDisposeTracker<Tag2>>(o => new DisposeTracker<Tag2>(o.GetService<IDisposeTracker<Tag1>>()));
        sc.AddTransient<IDisposeTracker<Tag3>>(o => new DisposeTracker<Tag3>(o.GetService<IDisposeTracker<Tag2>>()));
        sc.AddScoped<IDisposeTracker<Tag4>>(o => new DisposeTracker<Tag4>(o.GetService<IDisposeTracker<Tag3>>()));
        sc.AddTransient<IDisposeTracker<Tag5>>(o => new DisposeTracker<Tag5>(o.GetService<IDisposeTracker<Tag4>>()));
        DisposeTracker<Tag0> t0;
        DisposeTracker<Tag1> t1;
        DisposeTracker<Tag2> t2;
        DisposeTracker<Tag3> t3;
        DisposeTracker<Tag4> t4;
        DisposeTracker<Tag5> t5;
        {
            using var sp = new VisitableServiceProvider(sc);// sc.BuildServiceProvider();
            {
                using var scope = sp.CreateScope();
                t5 = (DisposeTracker<Tag5>)scope.ServiceProvider.GetService<IDisposeTracker<Tag5>>()!;
                t4 = (DisposeTracker<Tag4>)t5.Dependency!;
                t3 = (DisposeTracker<Tag3>)t4.Dependency!;
                t2 = (DisposeTracker<Tag2>)t3.Dependency!;
                t1 = (DisposeTracker<Tag1>)t2.Dependency!;
                t0 = (DisposeTracker<Tag0>)t1.Dependency!;
                t0.Dependency.Should().BeNull();

                t0.DisposeCount.Should().Be(0);
                t1.DisposeCount.Should().Be(0);
                t2.DisposeCount.Should().Be(0);
                t3.DisposeCount.Should().Be(0);
                t4.DisposeCount.Should().Be(0);
                t5.DisposeCount.Should().Be(0);
            }
            t0.DisposeCount.Should().Be(0);
            t1.DisposeCount.Should().Be(0);
            t2.DisposeCount.Should().Be(0);
            t3.DisposeCount.Should().Be(1);
            t4.DisposeCount.Should().Be(1);
            t5.DisposeCount.Should().Be(1);
        }
        t0.DisposeCount.Should().Be(1);
        t1.DisposeCount.Should().Be(1);
        t2.DisposeCount.Should().Be(1);
        t3.DisposeCount.Should().Be(1);
        t4.DisposeCount.Should().Be(1);
        t5.DisposeCount.Should().Be(1);
    }

    private class Tag0 { }
    private class Tag1 { }
    private class Tag2 { }
    private class Tag3 { }
    private class Tag4 { }
    private class Tag5 { }
}
