﻿namespace ServiceCollectionGraphTests.DependencyInjection.Extensions;

using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ServiceCollectionGraph.DependencyInjection.Extensions;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using static ServiceCollectionGraphTests.TestServices;

public class ServiceCollectionExtensionsTests
{
    private class FirstHostedService : IHostedService
    {
        public FirstHostedService(ILeafUser leafUser, ILeafUserUser leafUserUser)
        {
        }

        public Task StartAsync(CancellationToken cancellationToken) => Task.CompletedTask;
        public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;
    }

    private class SecondHostedService : IHostedService
    {
        public SecondHostedService(ILeafUser leafUser, ILeafUserUser leafUserUser)
        {
        }

        public Task StartAsync(CancellationToken cancellationToken) => Task.CompletedTask;
        public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;
    }

    [Fact]
    public void GeneratePlantUml_ForHostedServices_CreatesCorrectPlantUmlString()
    {
        var sc = new ServiceCollection();
        sc.AddSingleton<ILeaf, Leaf>();
        sc.AddTransient<ILeafUser, LeafUser>();
        sc.AddScoped<ILeafUserUser, LeafUserUser>();
        sc.AddHostedService<FirstHostedService>();
        sc.AddHostedService<SecondHostedService>();

        var actualPlantUml = sc.GeneratePlantUml(o => o.RootStereotypeName = "root");

        actualPlantUml.Should().Be(@"@startuml

hide circle
hide methods
hide stereotype

class ""FirstHostedService #0"" <<root>> {
    a : ILeafUser
    b : ILeafUserUser
}

class ""Leaf #0"" {
}

class ""LeafUser #0"" {
    a : ILeaf
}

class ""LeafUser #1"" {
    a : ILeaf
}

class ""LeafUser #2"" {
    a : ILeaf
}

class ""LeafUserUser #0"" {
    a : ILeafUser
}

class ""SecondHostedService #0"" <<root>> {
    a : ILeafUser
    b : ILeafUserUser
}

""FirstHostedService #0::a"" --> ""LeafUser #0""
""FirstHostedService #0::b"" --> ""LeafUserUser #0""
""LeafUser #0::a"" --> ""Leaf #0""
""LeafUser #1::a"" --> ""Leaf #0""
""LeafUser #2::a"" --> ""Leaf #0""
""LeafUserUser #0::a"" --> ""LeafUser #1""
""SecondHostedService #0::a"" --> ""LeafUser #2""
""SecondHostedService #0::b"" --> ""LeafUserUser #0""

@enduml
");
    }
}
