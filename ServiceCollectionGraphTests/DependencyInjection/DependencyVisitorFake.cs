﻿namespace ServiceCollectionGraphTests.DependencyInjection;

using ServiceCollectionGraph.DependencyInjection;
using System;
using System.Collections.Generic;

public class DependencyVisitorFake : IDependencyVisitor
{
    public record Call(Type? ServiceType, object? Service)
    {
        public static Call Begin(Type serviceType)
        {
            ArgumentNullException.ThrowIfNull(serviceType);
            return new Call(serviceType, null);
        }

        public static Call End(object? service)
        {
            return new Call(null, service);
        }
    }

    public List<Call> Calls { get; } = new();

    public void BeginResolveService(Type serviceType)
    {
        Calls.Add(Call.Begin(serviceType));
    }

    public void EndResolveService(object? service)
    {
        Calls.Add(Call.End(service));
    }
}
