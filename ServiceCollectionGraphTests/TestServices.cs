﻿namespace ServiceCollectionGraphTests;

using System;

internal static class TestServices
{
    public interface ILeaf
    {
    }

    public class Leaf : ILeaf
    {
    }

    public interface ILeafUser
    {
        ILeaf Leaf { get; }
    }

    public class LeafUser : ILeafUser
    {
        public LeafUser(ILeaf leaf)
        {
            Leaf = leaf;
        }

        public ILeaf Leaf { get; }
    }

    public interface ILeafUserUser
    {
        ILeafUser LeafUser { get; }
    }

    public class LeafUserUser : ILeafUserUser
    {
        public LeafUserUser(ILeafUser leafUser)
        {
            LeafUser = leafUser;
        }

        public ILeafUser LeafUser { get; }
    }

    public interface ITwiceUser
    {
        ILeafUser LeafUser { get; }
        ILeafUserUser LeafUserUser { get; }
    }

    public class TwiceUser : ITwiceUser
    {
        public TwiceUser(ILeafUser leafUser, ILeafUserUser leafUserUser)
        {
            LeafUser = leafUser;
            LeafUserUser = leafUserUser;
        }

        public ILeafUser LeafUser { get; }
        public ILeafUserUser LeafUserUser { get; }
    }

    public interface IGeneric<T>
    {
    }

    public class Generic<T> : IGeneric<T>
    {
    }

    public interface IGenericUser
    {
    }

    public class GenericUser : IGenericUser
    {
        public GenericUser(IGeneric<GenericUser> generic)
        {
            Generic = generic;
        }

        public IGeneric<GenericUser> Generic { get; }
    }

    public interface IDisposeTracker<T> : IDisposable
    {
        int DisposeCount { get; }
    }

    public sealed class DisposeTracker<T> : IDisposeTracker<T>
    {
        public DisposeTracker(object? dependency = null)
        {
            Dependency = dependency;
        }

        public object? Dependency { get; }
        public int DisposeCount { get; private set; }

        public void Dispose()
        {
            ++DisposeCount;
        }
    }
}
