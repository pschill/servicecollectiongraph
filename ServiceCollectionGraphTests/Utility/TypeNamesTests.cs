﻿namespace ServiceCollectionGraphTests.Utility;

using FluentAssertions;
using ServiceCollectionGraph.Utility;
using System;
using System.Collections.Generic;
using Xunit;

public class TypeNamesTests
{
    private class MyTestClass
    {
    }

    private interface IMyTestInterface
    {
    }

    private class Outer
    {
        public class Middle
        {
            public class Inner
            {
            }
        }
    }

    private class OuterGeneric<T>
    {
        public class MiddleGeneric<U>
        {
            public class InnerGeneric<V>
            {
            }
        }
    }

    [Theory]
    // non-generic types
    [InlineData(typeof(int), "Int32")]
    [InlineData(typeof(string), "String")]
    [InlineData(typeof(MyTestClass), "MyTestClass")]
    [InlineData(typeof(IMyTestInterface), "IMyTestInterface")]
    // generic types
    [InlineData(typeof(IEnumerable<int>), "IEnumerable<Int32>")]
    [InlineData(typeof(IEnumerable<List<string>>), "IEnumerable<List<String>>")]
    [InlineData(typeof(Dictionary<int, MyTestClass>), "Dictionary<Int32, MyTestClass>")]
    [InlineData(typeof(Dictionary<int, IList<IMyTestInterface>>), "Dictionary<Int32, IList<IMyTestInterface>>")]
    // nested types
    [InlineData(typeof(Outer.Middle.Inner), "Inner")]
    // special cases
    [InlineData(typeof(OuterGeneric<int>.MiddleGeneric<float>.InnerGeneric<string>), "InnerGeneric<String>")]
    public void GetTypeNameWithGenericArguments_ReturnsCorrectValues(Type type, string expectedName)
    {
        var actualName = TypeNames.GetTypeNameWithGenericArguments(type);
        actualName.Should().Be(expectedName);
    }

    [Theory]
    // non-generic types
    [InlineData(typeof(int), "Int32")]
    // generic types
    [InlineData(typeof(IEnumerable<int>), "IEnumerable<Int32>")]
    // nested types
    [InlineData(typeof(Outer.Middle.Inner), "TypeNamesTests.Outer.Middle.Inner")]
    // special cases
    [InlineData(typeof(OuterGeneric<int>.MiddleGeneric<float>.InnerGeneric<string>), "TypeNamesTests.OuterGeneric<Int32>.MiddleGeneric<Single>.InnerGeneric<String>")]
    public void GetNestedTypeNameWithGenericArguments_ReturnsCorrectValues(Type type, string expectedName)
    {
        var actualName = TypeNames.GetNestedTypeNameWithGenericArguments(type);
        actualName.Should().Be(expectedName);
    }
}
