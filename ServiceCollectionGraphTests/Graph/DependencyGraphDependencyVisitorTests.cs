﻿namespace ServiceCollectionGraphTests.Graph;

using FluentAssertions;
using ServiceCollectionGraph.Graph;
using Xunit;
using static ServiceCollectionGraphTests.TestServices;

public class DependencyGraphDependencyVisitorTests
{
    [Fact]
    public void AddExampleServices_ConstructsCorrectGraph()
    {
        var leaf = new Leaf();
        var firstLeafUser = new LeafUser(leaf);
        var secondLeafUser = new LeafUser(leaf);
        var leafUserUser = new LeafUserUser(secondLeafUser);
        var firstTwiceUser = new TwiceUser(firstLeafUser, leafUserUser);
        var thirdLeafUser = new LeafUser(leaf);
        var secondTwiceUser = new TwiceUser(thirdLeafUser, leafUserUser);

        var graph = new DependencyGraph();
        var visitor = new DependencyGraphDependencyVisitor(graph);

        visitor.BeginResolveService(typeof(ITwiceUser));
        visitor.BeginResolveService(typeof(ILeafUser));
        visitor.BeginResolveService(typeof(ILeaf));
        visitor.EndResolveService(leaf);
        visitor.EndResolveService(firstLeafUser);
        visitor.BeginResolveService(typeof(ILeafUserUser));
        visitor.BeginResolveService(typeof(ILeafUser));
        visitor.BeginResolveService(typeof(ILeaf));
        visitor.EndResolveService(leaf);
        visitor.EndResolveService(secondLeafUser);
        visitor.EndResolveService(leafUserUser);
        visitor.EndResolveService(firstTwiceUser);

        visitor.BeginResolveService(typeof(ITwiceUser));
        visitor.BeginResolveService(typeof(ILeafUser));
        visitor.BeginResolveService(typeof(ILeaf));
        visitor.EndResolveService(leaf);
        visitor.EndResolveService(thirdLeafUser);
        visitor.BeginResolveService(typeof(ILeafUserUser));
        visitor.EndResolveService(leafUserUser);
        visitor.EndResolveService(secondTwiceUser);

        graph.GetRoots().Should().Equal(
            new DependencyNode(typeof(ITwiceUser), firstTwiceUser),
            new DependencyNode(typeof(ITwiceUser), secondTwiceUser));
        graph.GetDependencies(firstTwiceUser).Should().Equal(
            new DependencyNode(typeof(ILeafUser), firstLeafUser),
            new DependencyNode(typeof(ILeafUserUser), leafUserUser));
        graph.GetDependencies(secondTwiceUser).Should().Equal(
            new DependencyNode(typeof(ILeafUser), thirdLeafUser),
            new DependencyNode(typeof(ILeafUserUser), leafUserUser));
        graph.GetDependencies(leafUserUser).Should().Equal(
            new DependencyNode(typeof(ILeafUser), secondLeafUser));
        graph.GetDependencies(firstLeafUser).Should().Equal(
            new DependencyNode(typeof(ILeaf), leaf));
        graph.GetDependencies(secondLeafUser).Should().Equal(
            new DependencyNode(typeof(ILeaf), leaf));
        graph.GetDependencies(thirdLeafUser).Should().Equal(
            new DependencyNode(typeof(ILeaf), leaf));
        graph.GetDependencies(leaf).Should().BeEmpty();
    }

    [Fact]
    public void SameServiceWithDifferentTypes_DoesNotAddDependencyToItself()
    {
        // A single service instance can be exposed with multiple interfaces using implementation factories.
        // In such cases, previous versions of the DependencyGraphDependencyVisitor added a dependency from the
        // instance to itself to the dependency graph.
        // services.AddSingleton<LeafUser>();
        // services.AddSingleton<ILeafUser>(o => o.GetRequiredService<LeafUser>());

        var graph = new DependencyGraph();
        var visitor = new DependencyGraphDependencyVisitor(graph);

        var leaf = new Leaf();
        var leafUser = new LeafUser(leaf);
        visitor.BeginResolveService(typeof(ILeafUser));
        visitor.BeginResolveService(typeof(LeafUser));
        visitor.BeginResolveService(typeof(ILeaf));
        visitor.EndResolveService(leaf);
        visitor.EndResolveService(leafUser);
        visitor.EndResolveService(leafUser);

        graph.GetRoots().Should().Equal(
            new DependencyNode(typeof(ILeafUser), leafUser));
        graph.GetDependencies(leafUser).Should().Equal(
            new DependencyNode(typeof(ILeaf), leaf));
        graph.GetDependencies(leaf).Should().BeEmpty();
    }
}
