﻿namespace ServiceCollectionGraphTests.Graph;

using FluentAssertions;
using ServiceCollectionGraph.Graph;
using Xunit;
using static ServiceCollectionGraphTests.TestServices;

public class DependencyGraphTests
{
    [Fact]
    public void ChangingRoots_WorksAsExpected()
    {
        var firstLeaf = new Leaf();
        var secondLeaf = new Leaf();
        var graph = new DependencyGraph();

        graph.AddRoot(new DependencyNode(typeof(ILeaf), firstLeaf));
        graph.GetRoots().Should().Equal(
            new DependencyNode(typeof(ILeaf), firstLeaf));

        graph.AddRoot(new DependencyNode(typeof(ILeaf), secondLeaf));
        graph.GetRoots().Should().Equal(
            new DependencyNode(typeof(ILeaf), firstLeaf),
            new DependencyNode(typeof(ILeaf), secondLeaf));

        graph.AddRoot(new DependencyNode(typeof(ILeaf), firstLeaf));
        graph.GetRoots().Should().Equal(
            new DependencyNode(typeof(ILeaf), firstLeaf),
            new DependencyNode(typeof(ILeaf), secondLeaf),
            new DependencyNode(typeof(ILeaf), firstLeaf));

        var firstRemoveResult = graph.RemoveRoot(new DependencyNode(typeof(ILeaf), firstLeaf));
        firstRemoveResult.Should().BeTrue();
        graph.GetRoots().Should().Equal(
            new DependencyNode(typeof(ILeaf), secondLeaf),
            new DependencyNode(typeof(ILeaf), firstLeaf));

        var secondRemoveResult = graph.RemoveRoot(new DependencyNode(typeof(ILeaf), firstLeaf));
        secondRemoveResult.Should().BeTrue();
        graph.GetRoots().Should().Equal(
            new DependencyNode(typeof(ILeaf), secondLeaf));

        var thirdRemoveResult = graph.RemoveRoot(new DependencyNode(typeof(ILeaf), secondLeaf));
        thirdRemoveResult.Should().BeTrue();
        graph.GetRoots().Should().BeEmpty();
    }

    [Fact]
    public void RemoveRoot_RootNodeDoesNotExist_ReturnsFalse()
    {
        var leaf = new Leaf();
        var graph = new DependencyGraph();
        graph.AddRoot(new DependencyNode(typeof(ILeaf), leaf));

        var removeResult = graph.RemoveRoot(new DependencyNode(typeof(ILeaf), new Leaf()));
        removeResult.Should().BeFalse();

        graph.GetRoots().Should().Equal(new DependencyNode(typeof(ILeaf), leaf));
    }

    [Fact]
    public void ChangingDependencies_WorksAsExpected()
    {
        var service = new object();
        var firstLeaf = new Leaf();
        var secondLeaf = new Leaf();
        var graph = new DependencyGraph();

        graph.AddDependency(service, new DependencyNode(typeof(ILeaf), firstLeaf));
        graph.GetDependencies(service).Should().Equal(
            new DependencyNode(typeof(ILeaf), firstLeaf));

        graph.AddDependency(service, new DependencyNode(typeof(ILeaf), secondLeaf));
        graph.GetDependencies(service).Should().Equal(
            new DependencyNode(typeof(ILeaf), firstLeaf),
            new DependencyNode(typeof(ILeaf), secondLeaf));

        graph.AddDependency(service, new DependencyNode(typeof(ILeaf), firstLeaf));
        graph.GetDependencies(service).Should().Equal(
            new DependencyNode(typeof(ILeaf), firstLeaf),
            new DependencyNode(typeof(ILeaf), secondLeaf),
            new DependencyNode(typeof(ILeaf), firstLeaf));

        var firstRemoveResult = graph.RemoveDependency(service, new DependencyNode(typeof(ILeaf), firstLeaf));
        firstRemoveResult.Should().BeTrue();
        graph.GetDependencies(service).Should().Equal(
            new DependencyNode(typeof(ILeaf), secondLeaf),
            new DependencyNode(typeof(ILeaf), firstLeaf));

        var secondRemoveResult = graph.RemoveDependency(service, new DependencyNode(typeof(ILeaf), firstLeaf));
        secondRemoveResult.Should().BeTrue();
        graph.GetDependencies(service).Should().Equal(
            new DependencyNode(typeof(ILeaf), secondLeaf));

        var thirdRemoveResult = graph.RemoveDependency(service, new DependencyNode(typeof(ILeaf), secondLeaf));
        thirdRemoveResult.Should().BeTrue();
        graph.GetDependencies(service).Should().BeEmpty();
    }

    [Fact]
    public void AddDependencyToService_DoesNotChangeOtherService()
    {
        var firstService = new object();
        var secondService = new object();
        var firstLeaf = new Leaf();
        var secondLeaf = new Leaf();
        var graph = new DependencyGraph();

        graph.AddDependency(firstService, new DependencyNode(typeof(ILeaf), firstLeaf));
        graph.GetDependencies(firstService).Should().Equal(
            new DependencyNode(typeof(ILeaf), firstLeaf));
        graph.GetDependencies(secondService).Should().BeEmpty();

        graph.AddDependency(secondService, new DependencyNode(typeof(ILeaf), secondLeaf));
        graph.GetDependencies(firstService).Should().Equal(
            new DependencyNode(typeof(ILeaf), firstLeaf));
        graph.GetDependencies(secondService).Should().Equal(
            new DependencyNode(typeof(ILeaf), secondLeaf));

        var firstRemoveResult = graph.RemoveDependency(firstService, new DependencyNode(typeof(ILeaf), firstLeaf));
        firstRemoveResult.Should().BeTrue();
        graph.GetDependencies(firstService).Should().BeEmpty();
        graph.GetDependencies(secondService).Should().Equal(
            new DependencyNode(typeof(ILeaf), secondLeaf));

        var secondRemoveResult = graph.RemoveDependency(secondService, new DependencyNode(typeof(ILeaf), secondLeaf));
        secondRemoveResult.Should().BeTrue();
        graph.GetDependencies(firstService).Should().BeEmpty();
        graph.GetDependencies(secondService).Should().BeEmpty();
    }

    [Fact]
    public void RemoveDependency_DependencyNodeDoesNotExist_ReturnsFalse()
    {
        var service = new object();
        var leaf = new Leaf();
        var graph = new DependencyGraph();
        graph.AddDependency(service, new DependencyNode(typeof(ILeaf), leaf));

        var removeResult = graph.RemoveDependency(service, new DependencyNode(typeof(ILeaf), new Leaf()));
        removeResult.Should().BeFalse();

        graph.GetDependencies(service).Should().Equal(new DependencyNode(typeof(ILeaf), leaf));
    }

    [Fact]
    public void GetDependency_ServiceHasNotBeenAdded_ReturnsEmptyList()
    {
        var service = new object();
        var graph = new DependencyGraph();

        graph.GetDependencies(service).Should().BeEmpty();
    }

    [Fact]
    public void RemoveDependency_ServiceHasNotBeenAdded_ReturnsFalse()
    {
        var service = new object();
        var graph = new DependencyGraph();

        var removeResult = graph.RemoveDependency(service, new DependencyNode(typeof(ILeaf), new Leaf()));

        removeResult.Should().BeFalse();
    }
}
