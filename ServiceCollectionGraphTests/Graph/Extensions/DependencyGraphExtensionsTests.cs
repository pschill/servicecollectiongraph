﻿namespace ServiceCollectionGraphTests.Graph.Extensions;

using FluentAssertions;
using ServiceCollectionGraph.Graph;
using ServiceCollectionGraph.Graph.Extensions;
using Xunit;

public class DependencyGraphExtensionsTests
{
    private class T0 { }
    private class T1 { }
    private class T2 { }
    private class T3 { }
    private class T4 { }
    private class T5 { }

    [Fact]
    public void CutRoots_ChangesTheNodes()
    {
        var root0 = new object();
        var root1 = new object();
        var inner0 = new object();
        var inner1 = new object();
        var inner2 = new object();
        var leaf = new object();
        var graph = new DependencyGraph();
        graph.AddRoot(new DependencyNode(typeof(T0), root0));
        graph.AddRoot(new DependencyNode(typeof(T1), root1));
        graph.AddDependency(root0, new DependencyNode(typeof(T2), inner0));
        graph.AddDependency(root0, new DependencyNode(typeof(T3), inner1));
        graph.AddDependency(root1, new DependencyNode(typeof(T3), inner1));
        graph.AddDependency(root1, new DependencyNode(typeof(T4), inner2));
        graph.AddDependency(inner0, new DependencyNode(typeof(T5), leaf));
        graph.AddDependency(inner1, new DependencyNode(typeof(T5), leaf));

        graph.CutRoots();

        graph.GetRoots().Should().Equal(
            new DependencyNode(typeof(T2), inner0),
            new DependencyNode(typeof(T3), inner1),
            new DependencyNode(typeof(T4), inner2));
        graph.GetDependencies(inner0).Should().Equal(
            new DependencyNode(typeof(T5), leaf));
        graph.GetDependencies(inner1).Should().Equal(
            new DependencyNode(typeof(T5), leaf));
        graph.GetDependencies(inner2).Should().BeEmpty();
        graph.GetDependencies(leaf).Should().BeEmpty();
    }
}
