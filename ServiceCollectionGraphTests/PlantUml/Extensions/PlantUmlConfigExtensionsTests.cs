﻿namespace ServiceCollectionGraphTests.PlantUml.Extensions;

using FluentAssertions;
using ServiceCollectionGraph.Graph;
using ServiceCollectionGraph.PlantUml;
using ServiceCollectionGraph.PlantUml.Extensions;
using Xunit;

public class PlantUmlConfigExtensionsTests
{
    private enum MyTestEnum
    {
        DefaultValue
    }

    [Fact]
    public void UseRootNodeColor_WithColorAndStereotypeName_SetsConfigValues()
    {
        var config = new PlantUmlConfig();

        config.UseRootNodeColor("blue", "root");

        config.RootNodeColor.Should().Be("blue");
        config.RootStereotypeName.Should().Be("root");
    }

    [Fact]
    public void UseRootNodeColor_WithOnlyColor_SetsConfigValues()
    {
        var config = new PlantUmlConfig();

        config.UseRootNodeColor("blue");

        config.RootNodeColor.Should().Be("blue");
        config.RootStereotypeName.Should().Be("Root");
    }

    [Fact]
    public void UseRootNodeColor_WithoutArguments_SetsConfigValuesToDefault()
    {
        var config = new PlantUmlConfig();

        config.UseRootNodeColor();

        config.RootNodeColor.Should().Be("#FEDCDC");
        config.RootStereotypeName.Should().Be("Root");
    }

    [Theory]
    [InlineData("Int", 1, true)]
    [InlineData("String", 1, false)]
    [InlineData("String", "hello", true)]
    [InlineData("Int", "hello", false)]
    [InlineData("System", 1, true)]
    [InlineData("System", "hello", true)]
    [InlineData("PlantUml", 1, false)]
    [InlineData("TestEnum", MyTestEnum.DefaultValue, true)]
    [InlineData("PlantUml", MyTestEnum.DefaultValue, true)]
    [InlineData("Int", MyTestEnum.DefaultValue, false)]
    [InlineData(@"ServiceCollectionGraphTests\.PlantUml\.Extensions\.PlantUmlConfigExtensionsTests\.MyTestEnum", MyTestEnum.DefaultValue, true)]
    public void UseInstanceTypeRegexToIncludeNodes_MatchesCorrectValues(string pattern, object instance, bool expectedResult)
    {
        var config = new PlantUmlConfig().UseInstanceTypeRegexToIncludeNodes(pattern);
        var node = new DependencyNode(typeof(object), instance);

        var actualResult = config.IncludeNodePredicate!.Invoke(node);

        actualResult.Should().Be(expectedResult);
    }

    [Theory]
    [InlineData("Int", 1, true)]
    [InlineData("String", 1, false)]
    [InlineData("String", "hello", true)]
    [InlineData("Int", "hello", false)]
    [InlineData("System", 1, true)]
    [InlineData("System", "hello", true)]
    [InlineData("PlantUml", 1, false)]
    [InlineData("TestEnum", MyTestEnum.DefaultValue, true)]
    [InlineData("PlantUml", MyTestEnum.DefaultValue, true)]
    [InlineData("Int", MyTestEnum.DefaultValue, false)]
    [InlineData(@"ServiceCollectionGraphTests\.PlantUml\.Extensions\.PlantUmlConfigExtensionsTests\.MyTestEnum", MyTestEnum.DefaultValue, true)]
    public void UseInstanceTypeRegexToIncludeNodeChildren_MatchesCorrectValues(string pattern, object instance, bool expectedResult)
    {
        var config = new PlantUmlConfig().UseInstanceTypeRegexToIncludeNodeChildren(pattern);
        var node = new DependencyNode(typeof(object), instance);

        var actualResult = config.IncludeNodeChildrenPredicate!.Invoke(node);

        actualResult.Should().Be(expectedResult);
    }
}
