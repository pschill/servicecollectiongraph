﻿namespace ServiceCollectionGraphTests.PlantUml;

using FluentAssertions;
using ServiceCollectionGraph.Graph;
using ServiceCollectionGraph.PlantUml;
using System.Collections.Generic;
using Xunit;
using static ServiceCollectionGraphTests.TestServices;

public class PlantUmlGeneratorTests
{
    [Theory]
    [MemberData(nameof(GetUmlTestData))]
    public void Generate_ComputesCorrectString(DependencyGraph graph, PlantUmlConfig config, string expectedUml)
    {
        var actualUml = PlantUmlGenerator.Generate(graph, config);

        actualUml.Should().Be(expectedUml);
    }

    public static IEnumerable<object[]> GetUmlTestData()
    {
        foreach (var (graph, config, expectedUml) in GetTypedTestData())
        {
            yield return new object[] { graph, config, expectedUml };
        }
    }

    private static IEnumerable<(DependencyGraph, PlantUmlConfig, string)> GetTypedTestData()
    {
        yield return GetTestData_ExampleServices();
        yield return GetTestData_Generics();
        yield return GetTestData_WithRootStereotypeName();
        yield return GetTestData_WithRootNodeColor();
        yield return GetTestData_WithIncludeNodePredicate();
        yield return GetTestData_WithIncludeNodeChildrenPredicate();
    }

    private static (DependencyGraph, PlantUmlConfig, string) GetTestData_ExampleServices()
    {
        var leaf = new Leaf();
        var firstLeafUser = new LeafUser(leaf);
        var secondLeafUser = new LeafUser(leaf);
        var leafUserUser = new LeafUserUser(secondLeafUser);
        var firstTwiceUser = new TwiceUser(firstLeafUser, leafUserUser);
        var thirdLeafUser = new LeafUser(leaf);
        var secondTwiceUser = new TwiceUser(thirdLeafUser, leafUserUser);

        var graph = new DependencyGraph();
        graph.AddRoot(new DependencyNode(typeof(ITwiceUser), firstTwiceUser));
        graph.AddRoot(new DependencyNode(typeof(ITwiceUser), secondTwiceUser));
        graph.AddDependency(firstTwiceUser, new DependencyNode(typeof(ILeafUser), firstLeafUser));
        graph.AddDependency(firstTwiceUser, new DependencyNode(typeof(ILeafUserUser), leafUserUser));
        graph.AddDependency(secondTwiceUser, new DependencyNode(typeof(ILeafUser), thirdLeafUser));
        graph.AddDependency(secondTwiceUser, new DependencyNode(typeof(ILeafUserUser), leafUserUser));
        graph.AddDependency(leafUserUser, new DependencyNode(typeof(ILeafUser), secondLeafUser));
        graph.AddDependency(firstLeafUser, new DependencyNode(typeof(ILeaf), leaf));
        graph.AddDependency(secondLeafUser, new DependencyNode(typeof(ILeaf), leaf));
        graph.AddDependency(thirdLeafUser, new DependencyNode(typeof(ILeaf), leaf));

        var config = new PlantUmlConfig();

        var expectedUml = @"@startuml

hide circle
hide methods
hide stereotype

class ""Leaf #0"" {
}

class ""LeafUser #0"" {
    a : ILeaf
}

class ""LeafUser #1"" {
    a : ILeaf
}

class ""LeafUser #2"" {
    a : ILeaf
}

class ""LeafUserUser #0"" {
    a : ILeafUser
}

class ""TwiceUser #0"" {
    a : ILeafUser
    b : ILeafUserUser
}

class ""TwiceUser #1"" {
    a : ILeafUser
    b : ILeafUserUser
}

""LeafUser #0::a"" --> ""Leaf #0""
""LeafUser #1::a"" --> ""Leaf #0""
""LeafUser #2::a"" --> ""Leaf #0""
""LeafUserUser #0::a"" --> ""LeafUser #1""
""TwiceUser #0::a"" --> ""LeafUser #0""
""TwiceUser #0::b"" --> ""LeafUserUser #0""
""TwiceUser #1::a"" --> ""LeafUser #2""
""TwiceUser #1::b"" --> ""LeafUserUser #0""

@enduml
";

        return (graph, config, expectedUml);
    }

    private static (DependencyGraph, PlantUmlConfig, string) GetTestData_Generics()
    {
        var generic = new Generic<GenericUser>();
        var genericUser = new GenericUser(generic);

        var graph = new DependencyGraph();
        graph.AddRoot(new DependencyNode(typeof(IGenericUser), genericUser));
        graph.AddDependency(genericUser, new DependencyNode(typeof(IGeneric<GenericUser>), generic));

        var config = new PlantUmlConfig();

        var expectedUml = @"@startuml

hide circle
hide methods
hide stereotype

class ""Generic<GenericUser> #0"" {
}

class ""GenericUser #0"" {
    a : IGeneric<GenericUser>
}

""GenericUser #0::a"" --> ""Generic<GenericUser> #0""

@enduml
";

        return (graph, config, expectedUml);
    }

    private static (DependencyGraph, PlantUmlConfig, string) GetTestData_WithRootStereotypeName()
    {
        var leaf0 = new Leaf();
        var leaf1 = new Leaf();
        var leafUser0 = new LeafUser(leaf0);
        var leafUser1 = new LeafUser(leaf1);
        var graph = new DependencyGraph();
        graph.AddRoot(new DependencyNode(typeof(ILeafUser), leafUser0));
        graph.AddRoot(new DependencyNode(typeof(ILeafUser), leafUser1));
        graph.AddDependency(leafUser0, new DependencyNode(typeof(ILeaf), leaf0));
        graph.AddDependency(leafUser1, new DependencyNode(typeof(ILeaf), leaf1));
        var config = new PlantUmlConfig { RootStereotypeName = "root" };

        var expectedUml = @"@startuml

hide circle
hide methods
hide stereotype

class ""Leaf #0"" {
}

class ""Leaf #1"" {
}

class ""LeafUser #0"" <<root>> {
    a : ILeaf
}

class ""LeafUser #1"" <<root>> {
    a : ILeaf
}

""LeafUser #0::a"" --> ""Leaf #0""
""LeafUser #1::a"" --> ""Leaf #1""

@enduml
";

        return (graph, config, expectedUml);
    }

    private static (DependencyGraph, PlantUmlConfig, string) GetTestData_WithRootNodeColor()
    {
        var leaf = new Leaf();
        var graph = new DependencyGraph();
        graph.AddRoot(new DependencyNode(typeof(ILeaf), leaf));
        var config = new PlantUmlConfig
        {
            RootNodeColor = "blue",
            RootStereotypeName = "root"
        };

        var expectedUml = @"@startuml

hide circle
hide methods
hide stereotype

skinparam class {
    BackgroundColor<<root>> blue
}

class ""Leaf #0"" <<root>> {
}

@enduml
";

        return (graph, config, expectedUml);
    }

    private static (DependencyGraph, PlantUmlConfig, string) GetTestData_WithIncludeNodePredicate()
    {
        var leaf0 = new Leaf();
        var leaf1 = new Leaf();
        var leafUser0 = new LeafUser(leaf0);
        var leafUser1 = new LeafUser(leaf1);

        var graph = new DependencyGraph();
        graph.AddRoot(new DependencyNode(typeof(ILeafUser), leafUser0));
        graph.AddRoot(new DependencyNode(typeof(ILeafUser), leafUser1));
        graph.AddDependency(leafUser0, new DependencyNode(typeof(ILeaf), leaf0));
        graph.AddDependency(leafUser1, new DependencyNode(typeof(ILeaf), leaf1));

        var config = new PlantUmlConfig
        {
            IncludeNodePredicate = o => o.Service != leafUser1
        };

        var expectedUml = @"@startuml

hide circle
hide methods
hide stereotype

class ""Leaf #0"" {
}

class ""LeafUser #0"" {
    a : ILeaf
}

""LeafUser #0::a"" --> ""Leaf #0""

@enduml
";

        return (graph, config, expectedUml);
    }

    private static (DependencyGraph, PlantUmlConfig, string) GetTestData_WithIncludeNodeChildrenPredicate()
    {
        var leaf0 = new Leaf();
        var leaf1 = new Leaf();
        var leafUser0 = new LeafUser(leaf0);
        var leafUser1 = new LeafUser(leaf1);

        var graph = new DependencyGraph();
        graph.AddRoot(new DependencyNode(typeof(ILeafUser), leafUser0));
        graph.AddRoot(new DependencyNode(typeof(ILeafUser), leafUser1));
        graph.AddDependency(leafUser0, new DependencyNode(typeof(ILeaf), leaf0));
        graph.AddDependency(leafUser1, new DependencyNode(typeof(ILeaf), leaf1));

        var config = new PlantUmlConfig
        {
            IncludeNodeChildrenPredicate = o => o.Service != leafUser1
        };

        var expectedUml = @"@startuml

hide circle
hide methods
hide stereotype

class ""Leaf #0"" {
}

class ""LeafUser #0"" {
    a : ILeaf
}

class ""LeafUser #1"" {
    a : ILeaf
}

""LeafUser #0::a"" --> ""Leaf #0""

@enduml
";

        return (graph, config, expectedUml);
    }
}
