# ServiceCollectionGraph

This .NET library allows users to create a dependency graph from a C# IServiceCollection.

## Usage

Call the `GeneratePlantUml` extension method on the `IServiceCollection`:
```cs
IServiceCollection services = ...;
string plantUml = services.GeneratePlantUml();
```

Under the hood, this creates a custom service provider that instantiates all services of type `IHostedService`. While
doing so, the provider tracks the dependencies (and recursively their dependencies) of each service and builds the
graph.

The other overloads of `GeneratePlantUml` allow you to specify a `PlantUmlConfig`:
```cs
IServiceCollection services = ...;
string plantUml = services.GeneratePlantUml(config =>
{
    // Use PlantUML stereotype on the hosted services and add a color for the stereotype.
    config.UseRootNodeColor("#FEDCDC");

    // Only show classes whose namespace starts with "MyNamespace" or "System".
    config.UseInstanceTypeRegexToIncludeNodes("^MyNamespace|^System");

    // Only show the dependencies of classes whose namespace starts with "MyNamespace".
    config.UseInstanceTypeRegexToIncludeNodeChildren("^MyNamespace");
});
```
