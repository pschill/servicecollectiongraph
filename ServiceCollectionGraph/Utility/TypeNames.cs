﻿namespace ServiceCollectionGraph.Utility;

using System;
using System.Linq;

/// <summary>
/// Contains utility functions for type names.
/// </summary>
public static class TypeNames
{
    /// <summary>
    /// Returns the name of the given type with generic arguments, for example, "IList&lt;Int32&gt;".
    /// </summary>
    /// <param name="type">The type.</param>
    /// <returns>The type name.</returns>
    /// <exception cref="ArgumentNullException"><paramref name="type"/> is null.</exception>
    public static string GetTypeNameWithGenericArguments(Type type)
    {
        ArgumentNullException.ThrowIfNull(type);
        return GetTypeNameWithGenericArguments(type, type.GetGenericArguments());
    }

    private static string GetTypeNameWithGenericArguments(Type type, Type[] genericArguments)
    {
        ArgumentNullException.ThrowIfNull(type);
        if (type.IsArray)
        {
            var elementTypeName = GetTypeNameWithGenericArguments(type.GetElementType()!);
            return $"{elementTypeName}[]";
        }
        if (type.IsGenericType)
        {
            var rawName = type.Name.Substring(0, type.Name.IndexOf('`'));
            var genericArgumentsOffset = type.IsNested ? type.DeclaringType!.GetGenericArguments().Length : 0;
            var genericArgumentsCount = type.GetGenericArguments().Length - genericArgumentsOffset;
            var relevantGenericArguments = genericArguments.Skip(genericArgumentsOffset).Take(genericArgumentsCount);
            var genericArgumentsNames = string.Join(", ", relevantGenericArguments.Select(o => GetTypeNameWithGenericArguments(o)));
            return $"{rawName}<{genericArgumentsNames}>";
        }
        return type.Name;
    }

    /// <summary>
    /// Returns the name of the given type with generic arguments. If the type is nested within another type, the
    /// parent type is used as prefix, for example, "IList&lt;Int32&gt;.Enumerator".
    /// </summary>
    /// <param name="type">The type.</param>
    /// <returns>The full type name.</returns>
    public static string GetNestedTypeNameWithGenericArguments(Type type)
    {
        return GetNestedTypeNameWithGenericArguments(type, type.GetGenericArguments());
    }

    private static string GetNestedTypeNameWithGenericArguments(Type type, Type[] genericArguments)
    {
        ArgumentNullException.ThrowIfNull(type);
        var prefix = type.IsNested
            ? $"{GetNestedTypeNameWithGenericArguments(type.DeclaringType!, genericArguments)}."
            : "";
        return $"{prefix}{GetTypeNameWithGenericArguments(type, genericArguments)}";
    }
}
