﻿namespace ServiceCollectionGraph.PlantUml;

using ServiceCollectionGraph.Graph;
using ServiceCollectionGraph.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/// <summary>
/// Generates PlantUML from a <see cref="IReadOnlyDependencyGraph"/>.
/// </summary>
public static class PlantUmlGenerator
{
    /// <summary>
    /// Generates a PlantUML diagram for the given dependency graph.
    /// </summary>
    /// <param name="graph">The graph.</param>
    /// <param name="config">The config.</param>
    /// <returns>The PlantUML diagram.</returns>
    /// <exception cref="ArgumentNullException"><paramref name="graph"/> or <paramref name="config"/> is null.</exception>
    public static string Generate(IReadOnlyDependencyGraph graph, PlantUmlConfig config)
    {
        ArgumentNullException.ThrowIfNull(graph);
        ArgumentNullException.ThrowIfNull(config);
        return new Generator(graph, config).Generate();
    }

    private class NodeData
    {
        public NodeData(string serviceName)
        {
            ServiceName = serviceName;
        }

        public string ServiceName { get; }
        public List<DependencyNode> Dependencies { get; } = new();
    }

    private class Generator
    {
        private readonly IReadOnlyDependencyGraph _graph;
        private readonly PlantUmlConfig _config;
        private readonly Dictionary<object, NodeData> _serviceData = new();
        private readonly Dictionary<Type, int> _instanceCounts = new();

        public Generator(IReadOnlyDependencyGraph graph, PlantUmlConfig config)
        {
            _graph = graph ?? throw new ArgumentNullException(nameof(graph));
            _config = config ?? throw new ArgumentNullException(nameof(config));

            Initialize();
        }

        public string Generate()
        {
            // Add preamble.
            var sb = new StringBuilder();
            sb.AppendLine("@startuml");
            sb.AppendLine("");
            sb.AppendLine("hide circle");
            sb.AppendLine("hide methods");
            sb.AppendLine("hide stereotype");
            sb.AppendLine("");
            var hasRootStereotype = !string.IsNullOrWhiteSpace(_config.RootStereotypeName);
            if (!string.IsNullOrWhiteSpace(_config.RootStereotypeName)
                && !string.IsNullOrWhiteSpace(_config.RootNodeColor))
            {
                sb.AppendLine("skinparam class {");
                sb.AppendLine($"    BackgroundColor<<{_config.RootStereotypeName}>> {_config.RootNodeColor}");
                sb.AppendLine("}");
                sb.AppendLine("");
            }

            // Add classes.
            var orderedServices = _serviceData.OrderBy(o => o.Value.ServiceName).ToList();
            var roots = _graph.GetRoots().Select(o => o.Service).ToHashSet();
            foreach (var (service, serviceData) in orderedServices)
            {
                var stereotype = hasRootStereotype && roots.Contains(service)
                    ? $" <<{_config.RootStereotypeName}>>"
                    : "";
                sb.AppendLine($"class \"{serviceData.ServiceName}\"{stereotype} {{");

                // List all dependencies as members, even if their nodes are not expanded in the diagram.
                var memberIndex = 0;
                foreach (var dependency in _graph.GetDependencies(service))
                {
                    var dependencyTypeName = TypeNames.GetTypeNameWithGenericArguments(dependency.ServiceType);
                    var memberName = GetMemberName(memberIndex++);
                    sb.AppendLine($"    {memberName} : {dependencyTypeName}");

                }
                sb.AppendLine("}");
                sb.AppendLine("");
            }

            // Add relations.
            var hasRelations = false;
            foreach (var (service, serviceData) in orderedServices)
            {
                var memberIndex = 0;
                foreach (var dependency in serviceData.Dependencies)
                {
                    var dependencyName = _serviceData[dependency.Service].ServiceName;
                    var memberName = GetMemberName(memberIndex++);
                    sb.AppendLine($"\"{serviceData.ServiceName}::{memberName}\" --> \"{dependencyName}\"");
                    hasRelations = true;
                }
            }
            if (hasRelations)
            {
                sb.AppendLine("");
            }

            sb.AppendLine("@enduml");
            return sb.ToString();
        }

        private void Initialize()
        {
            foreach (var rootNode in _graph.GetRoots())
            {
                var includeNode = _config.IncludeNodePredicate?.Invoke(rootNode) ?? true;
                if (includeNode)
                {
                    AddRecursively(rootNode);
                }
            }
        }

        private void AddRecursively(DependencyNode node)
        {
            ArgumentNullException.ThrowIfNull(node);
            if (_serviceData.ContainsKey(node.Service))
            {
                return;
            }

            var instanceType = node.Service.GetType();
            var instanceIndex = _instanceCounts.GetValueOrDefault(instanceType, 0);
            _instanceCounts[instanceType] = instanceIndex + 1;
            var nodeData = new NodeData(GetServiceNameWithIndex(instanceType, instanceIndex));
            _serviceData.Add(node.Service, nodeData);

            var includeNodeChildren = _config.IncludeNodeChildrenPredicate?.Invoke(node) ?? true;
            if (includeNodeChildren)
            {
                foreach (var dependency in _graph.GetDependencies(node.Service))
                {
                    var includeChild = _config.IncludeNodePredicate?.Invoke(dependency) ?? true;
                    if (includeChild)
                    {
                        nodeData.Dependencies.Add(dependency);
                        AddRecursively(dependency);
                    }
                }
            }
        }

        private static string GetServiceNameWithIndex(Type type, int index)
        {
            ArgumentNullException.ThrowIfNull(type);
            return $"{TypeNames.GetTypeNameWithGenericArguments(type)} #{index}";
        }

        private static string GetMemberName(int index)
        {
            var sb = new StringBuilder();
            while (index > 26)
            {
                sb.Append((char)('a' + (index % 26)));
                index /= 26;
            }
            sb.Append((char)('a' + (index % 26)));

            var arr = sb.ToString().ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }
    }
}
