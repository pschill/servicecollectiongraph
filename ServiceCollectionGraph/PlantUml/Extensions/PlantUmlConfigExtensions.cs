﻿namespace ServiceCollectionGraph.PlantUml.Extensions;

using ServiceCollectionGraph.PlantUml;
using ServiceCollectionGraph.Utility;
using System;
using System.Text.RegularExpressions;

/// <summary>
/// Contains extension methods for <see cref="PlantUmlConfig"/>.
/// </summary>
public static class PlantUmlConfigExtensions
{
    /// <summary>
    /// Sets <see cref="PlantUmlConfig.RootNodeColor"/> and <see cref="PlantUmlConfig.RootStereotypeName"/> to the
    /// given values.
    /// </summary>
    /// <param name="config">The config whose values are set.</param>
    /// <param name="rootNodeColor">The root node color.</param>
    /// <param name="rootStereotypeName">The stereotype name for root nodes.</param>
    /// <returns>The config.</returns>
    /// <exception cref="ArgumentNullException"><paramref name="config"/> is null.</exception>
    public static PlantUmlConfig UseRootNodeColor(this PlantUmlConfig config, string rootNodeColor = "#FEDCDC", string rootStereotypeName = "Root")
    {
        ArgumentNullException.ThrowIfNull(config);
        config.RootNodeColor = rootNodeColor;
        config.RootStereotypeName = rootStereotypeName;
        return config;
    }

    /// <summary>
    /// Sets <see cref="PlantUmlConfig.IncludeNodePredicate"/> to a predicate that includes nodes whose instance type,
    /// prefixed with the namespace, matches the regular expression with the given pattern.
    /// </summary>
    /// <param name="config">The config whose values are set.</param>
    /// <param name="pattern">The pattern.</param>
    /// <returns>The config.</returns>
    /// <exception cref="ArgumentNullException"><paramref name="config"/> or <paramref name="pattern"/> is null.</exception>
    public static PlantUmlConfig UseInstanceTypeRegexToIncludeNodes(this PlantUmlConfig config, string pattern)
    {
        ArgumentNullException.ThrowIfNull(config);
        ArgumentNullException.ThrowIfNull(pattern);
        return config.UseInstanceTypeRegexToIncludeNodes(new Regex(pattern));
    }

    /// <summary>
    /// Sets <see cref="PlantUmlConfig.IncludeNodePredicate"/> to a predicate that includes nodes whose instance type,
    /// prefixed with the namespace, matches the given regular expression.
    /// </summary>
    /// <param name="config">The config whose values are set.</param>
    /// <param name="regex">The regular expression.</param>
    /// <returns>The config.</returns>
    /// <exception cref="ArgumentNullException"><paramref name="config"/> or <paramref name="regex"/> is null.</exception>
    public static PlantUmlConfig UseInstanceTypeRegexToIncludeNodes(this PlantUmlConfig config, Regex regex)
    {
        ArgumentNullException.ThrowIfNull(config);
        ArgumentNullException.ThrowIfNull(regex);
        config.IncludeNodePredicate = o =>
        {
            var instanceType = o.Service.GetType();
            var name = $"{instanceType.Namespace}.{TypeNames.GetNestedTypeNameWithGenericArguments(instanceType)}";
            return regex.IsMatch(name);
        };
        return config;
    }

    /// <summary>
    /// Sets <see cref="PlantUmlConfig.IncludeNodeChildrenPredicate"/> to a predicate that includes nodes whose
    /// instance type, prefixed with the namespace, matches the regular expression with the given pattern.
    /// </summary>
    /// <param name="config">The config whose values are set.</param>
    /// <param name="pattern">The pattern.</param>
    /// <returns>The config.</returns>
    /// <exception cref="ArgumentNullException"><paramref name="config"/> or <paramref name="pattern"/> is null.</exception>
    public static PlantUmlConfig UseInstanceTypeRegexToIncludeNodeChildren(this PlantUmlConfig config, string pattern)
    {
        ArgumentNullException.ThrowIfNull(config);
        ArgumentNullException.ThrowIfNull(pattern);
        return config.UseInstanceTypeRegexToIncludeNodeChildren(new Regex(pattern));
    }

    /// <summary>
    /// Sets <see cref="PlantUmlConfig.IncludeNodeChildrenPredicate"/> to a predicate that includes nodes whose
    /// instance type, prefixed with the namespace, matches the given regular expression.
    /// </summary>
    /// <param name="config">The config whose values are set.</param>
    /// <param name="regex">The regular expression.</param>
    /// <returns>The config.</returns>
    /// <exception cref="ArgumentNullException"><paramref name="config"/> or <paramref name="regex"/> is null.</exception>
    public static PlantUmlConfig UseInstanceTypeRegexToIncludeNodeChildren(this PlantUmlConfig config, Regex regex)
    {
        ArgumentNullException.ThrowIfNull(config);
        ArgumentNullException.ThrowIfNull(regex);
        config.IncludeNodeChildrenPredicate = o =>
        {
            var instanceType = o.Service.GetType();
            var name = $"{instanceType.Namespace}.{TypeNames.GetNestedTypeNameWithGenericArguments(instanceType)}";
            return regex.IsMatch(name);
        };
        return config;
    }
}
