﻿namespace ServiceCollectionGraph.PlantUml;

using ServiceCollectionGraph.Graph;
using System;

/// <summary>
/// Config for the <see cref="PlantUmlGenerator"/>.
/// </summary>
public class PlantUmlConfig
{
    /// <summary>
    /// Name of the stereotype that should be used for root nodes. No stereotype will be used if this is null or
    /// whitespace.
    /// </summary>
    public string? RootStereotypeName { get; set; }

    /// <summary>
    /// Background color that should be used for root nodes. No color will be used if this is null or whitespace.
    /// Requires <see cref="RootStereotypeName"/> to be set.
    /// </summary>
    public string? RootNodeColor { get; set; }

    /// <summary>
    /// Predicate for the nodes that should be included in the diagram. The predicate should be true for nodes that are
    /// included in the diagram and false for nodes that should be excluded. If the predicate is null, all nodes are
    /// included.
    /// </summary>
    public Predicate<DependencyNode>? IncludeNodePredicate { get; set; }

    /// <summary>
    /// Predicate for the nodes whose children should be included in the diagram. The predicate should be true for
    /// nodes whose children should be included in the diagram and false for nodes whose children should be excluded.
    /// If the predicate is null, all children of all nodes are included.
    /// </summary>
    public Predicate<DependencyNode>? IncludeNodeChildrenPredicate { get; set; }
}
