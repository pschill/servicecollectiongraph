﻿namespace ServiceCollectionGraph.DependencyInjection;

using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

/// <summary>
/// A service provider that, when creating a service, calls a visitor on each of the service's dependency.
/// It can be used to generate the dependency graph of a service collection.
/// </summary>
public class VisitableServiceProvider : IServiceProvider, IServiceScopeFactory, IDisposable
{
    private readonly List<ServiceDescriptor> _services;
    private readonly IDependencyVisitor? _visitor;
    private readonly ServiceContainer _singletonServices;
    private readonly ServiceContainer _scopedServices;
    private bool _isDisposed = false;
    private bool _isResolvingSingleton = false;

    /// <summary>
    /// Constructs the service provider for the given service collection using the given visitor.
    /// </summary>
    /// <param name="services">The service collection.</param>
    /// <param name="visitor">The visitor.</param>
    /// <exception cref="ArgumentNullException"><paramref name="services"/> or <paramref name="visitor"/> is null.</exception>
    public VisitableServiceProvider(
        ICollection<ServiceDescriptor> services,
        IDependencyVisitor? visitor = null)
    {
        _services = services?.ToList() ?? throw new ArgumentNullException(nameof(services));
        _visitor = visitor;
        _singletonServices = new(this);
        _scopedServices = new(this);
    }

    private VisitableServiceProvider(
        List<ServiceDescriptor> services,
        IDependencyVisitor? visitor,
        ServiceContainer singletonServices)
    {
        _services = services ?? throw new ArgumentNullException(nameof(services));
        _visitor = visitor;
        _singletonServices = singletonServices ?? throw new ArgumentNullException(nameof(singletonServices));
        _scopedServices = new(this);
    }

    /// <inheritdoc/>
    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(isSafeToDisposeManagedObject: true);
        GC.SuppressFinalize(this);
    }

    /// <inheritdoc/>
    public IServiceScope CreateScope()
    {
        var scopedProvider = new VisitableServiceProvider(_services, _visitor, _singletonServices);
        return scopedProvider._scopedServices;
    }

    /// <inheritdoc/>
    public object? GetService(Type serviceType)
    {
        ArgumentNullException.ThrowIfNull(serviceType);
        _visitor?.BeginResolveService(serviceType);
        object? service = null;
        try
        {
            service = ResolveFromType(serviceType);
        }
        finally
        {
            _visitor?.EndResolveService(service);
        }
        return service;
    }

    /// <summary>
    /// Protected virtual part of the dispose pattern.
    /// </summary>
    /// <param name="isSafeToDisposeManagedObject">True if it is safe to dispose managed objects, otherwise false.</param>
    protected virtual void Dispose(bool isSafeToDisposeManagedObject)
    {
        if (!_isDisposed)
        {
            if (isSafeToDisposeManagedObject)
            {
                _scopedServices.Dispose();
                _singletonServices.Dispose();
            }
            _isDisposed = true;
        }
    }

    private object? ResolveFromType(Type serviceType)
    {
        if (serviceType == typeof(IServiceProvider) || serviceType == typeof(IServiceScopeFactory))
        {
            return this;
        }

        var (matchingDescriptor, matchingKey) = FindAllMatchingServiceDescriptors(serviceType).LastOrDefault();
        if (matchingDescriptor is not null)
        {
            return ResolveFromDescriptor(serviceType, matchingDescriptor, matchingKey);
        }

        if (serviceType.IsGenericType && serviceType.GetGenericTypeDefinition() == typeof(IEnumerable<>))
        {
            var itemType = serviceType.GetGenericArguments()[0];
            var itemMatches = FindAllMatchingServiceDescriptors(itemType).ToList();
            var serviceArray = Array.CreateInstance(itemType, itemMatches.Count);
            foreach (var (itemDescriptor, itemKey) in itemMatches)
            {
                _visitor?.BeginResolveService(itemType);
                object? item = null;
                try
                {
                    item = ResolveFromDescriptor(itemType, itemDescriptor, itemKey);
                    serviceArray.SetValue(item, itemKey.Slot);
                }
                finally
                {
                    _visitor?.EndResolveService(item);
                }
            }
            return serviceArray;
        }

        return null;
    }

    private object ResolveFromDescriptor(Type serviceType, ServiceDescriptor descriptor, ServiceKey key)
    {
        Debug.Assert(descriptor.Lifetime == ServiceLifetime.Singleton
            || descriptor.Lifetime == ServiceLifetime.Scoped
            || descriptor.Lifetime == ServiceLifetime.Transient);

        var scope = _isResolvingSingleton || descriptor.Lifetime == ServiceLifetime.Singleton
            ? _singletonServices
            : _scopedServices;
        if (descriptor.Lifetime != ServiceLifetime.Transient
            && scope.TryGetService(key, out var resolvedService))
        {
            return resolvedService;
        }

        var wasResolvingSingleton = _isResolvingSingleton;
        _isResolvingSingleton = _isResolvingSingleton || descriptor.Lifetime == ServiceLifetime.Singleton;
        object? service;
        try
        {
            service = CreateFromDescriptor(serviceType, descriptor);
        }
        finally
        {
            _isResolvingSingleton = wasResolvingSingleton;
        }

        if (descriptor.Lifetime == ServiceLifetime.Transient)
        {
            scope.AddTransientService(service);
        }
        else
        {
            scope.AddSingletonOrScopedService(key, service);
        }
        return service;
    }

    private object CreateFromDescriptor(Type serviceType, ServiceDescriptor descriptor)
    {
        if (descriptor.ImplementationInstance is not null)
        {
            if (!serviceType.IsAssignableFrom(descriptor.ImplementationInstance.GetType()))
            {
                throw new InvalidOperationException("Implementation instance of service descriptor has wrong type.");
            }
            return descriptor.ImplementationInstance;
        }

        if (descriptor.ImplementationFactory is not null)
        {
            return descriptor.ImplementationFactory.Invoke(this);
        }

        if (descriptor.ImplementationType is not null)
        {
            var closedType = descriptor.ImplementationType.IsGenericTypeDefinition
                ? descriptor.ImplementationType.MakeGenericType(serviceType.GenericTypeArguments)
                : descriptor.ImplementationType;
            return ActivatorUtilities.CreateInstance(this, closedType);
        }

        throw new InvalidOperationException("Service descriptor has neither implementation instance, implementation factory, nor implementation type.");
    }

    private IEnumerable<(ServiceDescriptor, ServiceKey)> FindAllMatchingServiceDescriptors(Type serviceType)
    {
        var count = 0;
        foreach (var desc in _services)
        {
            if (desc.ServiceType == serviceType
                || serviceType.IsGenericType && desc.ServiceType == serviceType.GetGenericTypeDefinition())
            {
                yield return (desc, new ServiceKey(serviceType, count));
                ++count;
            }
        }
    }
}
