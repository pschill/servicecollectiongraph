﻿namespace ServiceCollectionGraph.DependencyInjection;

using System;

internal readonly record struct ServiceKey(Type Type, int Slot);
