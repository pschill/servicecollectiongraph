﻿namespace ServiceCollectionGraph.DependencyInjection;

using System;

/// <summary>
/// Visitor for the <see cref="VisitableServiceProvider"/>.
/// </summary>
public interface IDependencyVisitor
{
    /// <summary>
    /// This is called when trying to create an object for a given service type.
    /// </summary>
    /// <param name="serviceType">The service type.</param>
    void BeginResolveService(Type serviceType);

    /// <summary>
    /// This is called when an object for a given service type has been created.
    /// </summary>
    /// <param name="service">The created service, or null if no service has been created.</param>
    void EndResolveService(object? service);
}
