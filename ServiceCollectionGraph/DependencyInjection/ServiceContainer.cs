﻿namespace ServiceCollectionGraph.DependencyInjection;

using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

internal sealed class ServiceContainer : IServiceScope
{
    private readonly Dictionary<ServiceKey, object> _resolvedServices = new();
    private readonly List<IDisposable> _disposables = new();
    private bool _isDisposed = false;

    public ServiceContainer(IServiceProvider serviceProvider)
    {
        ServiceProvider = serviceProvider ?? throw new ArgumentNullException(nameof(serviceProvider));
    }

    public IServiceProvider ServiceProvider { get; }

    public void AddSingletonOrScopedService(ServiceKey key, object service)
    {
        ThrowIfDisposed();
        _resolvedServices.Add(key, service);
        if (service is IDisposable disposable)
        {
            _disposables.Add(disposable);
        }
    }

    public void AddTransientService(object service)
    {
        ThrowIfDisposed();
        if (service is IDisposable disposable)
        {
            _disposables.Add(disposable);
        }
    }

    public bool TryGetService(ServiceKey key, [MaybeNullWhen(false)] out object service)
    {
        ThrowIfDisposed();
        return _resolvedServices.TryGetValue(key, out service);
    }

    public void Dispose()
    {
        if (!_isDisposed)
        {
            _resolvedServices.Clear();
            foreach (var disposable in _disposables)
            {
                disposable.Dispose();
            }
            _disposables.Clear();
            _isDisposed = true;
        }
    }

    private void ThrowIfDisposed()
    {
        if (_isDisposed)
        {
            throw new ObjectDisposedException(GetType().FullName);
        }
    }
}
