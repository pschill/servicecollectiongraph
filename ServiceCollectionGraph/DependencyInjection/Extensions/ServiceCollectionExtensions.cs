﻿namespace ServiceCollectionGraph.DependencyInjection.Extensions;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ServiceCollectionGraph.Graph;
using ServiceCollectionGraph.Graph.Extensions;
using ServiceCollectionGraph.PlantUml;
using System;
using System.Collections.Generic;

/// <summary>
/// Contains extension methods for <see cref="IServiceCollection"/>.
/// </summary>
public static class ServiceCollectionExtensions
{
    /// <summary>
    /// Generates PlantUML for the hosted services of the given service collection using a default constructed
    /// <see cref="PlantUmlConfig"/>.
    /// </summary>
    /// <param name="services">The service collection.</param>
    /// <returns>The PlantUML string.</returns>
    /// <exception cref="ArgumentNullException"><paramref name="services"/> is null.</exception>
    public static string GeneratePlantUml(this IServiceCollection services)
    {
        ArgumentNullException.ThrowIfNull(services);
        var config = new PlantUmlConfig();
        return GeneratePlantUml(services, config);
    }

    /// <summary>
    /// Generates PlantUML for the hosted services of the given service collection.
    /// </summary>
    /// <param name="services">The service collection.</param>
    /// <param name="configureOptions">The action used to configure the options.</param>
    /// <returns>The PlantUML string.</returns>
    /// <exception cref="ArgumentNullException"><paramref name="services"/> or <paramref name="configureOptions"/> is null.</exception>
    public static string GeneratePlantUml(this IServiceCollection services, Action<PlantUmlConfig> configureOptions)
    {
        ArgumentNullException.ThrowIfNull(services);
        ArgumentNullException.ThrowIfNull(configureOptions);
        var config = new PlantUmlConfig();
        configureOptions.Invoke(config);
        return GeneratePlantUml(services, config);
    }

    /// <summary>
    /// Generates PlantUML for the hosted services of the given service collection.
    /// </summary>
    /// <param name="services">The service collection.</param>
    /// <param name="config">The PlantUML configuration.</param>
    /// <returns>The PlantUML string.</returns>
    /// <exception cref="ArgumentNullException"><paramref name="services"/> or <paramref name="config"/> is null.</exception>
    public static string GeneratePlantUml(this IServiceCollection services, PlantUmlConfig config)
    {
        ArgumentNullException.ThrowIfNull(services);
        ArgumentNullException.ThrowIfNull(config);
        var graph = new DependencyGraph();
        var visitor = new DependencyGraphDependencyVisitor(graph);
        using var serviceProvider = new VisitableServiceProvider(services, visitor);
        serviceProvider.GetService<IEnumerable<IHostedService>>();
        graph.CutRoots();
        return PlantUmlGenerator.Generate(graph, config);
    }
}
