﻿namespace ServiceCollectionGraph.Graph;

using System;
using System.Collections.Generic;

/// <summary>
/// Read-only interface of the dependency graph of a service collection.
/// </summary>
public interface IReadOnlyDependencyGraph
{
    /// <summary>
    /// Returns all root nodes.
    /// </summary>
    /// <returns>The root nodes.</returns>
    IEnumerable<DependencyNode> GetRoots();

    /// <summary>
    /// Returns all dependencies of the given service.
    /// </summary>
    /// <param name="service">The service.</param>
    /// <returns>The dependencies of the service.</returns>
    /// <exception cref="ArgumentNullException"><paramref name="service"/> is null.</exception>
    IEnumerable<DependencyNode> GetDependencies(object service);
}
