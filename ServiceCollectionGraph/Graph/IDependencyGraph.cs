﻿namespace ServiceCollectionGraph.Graph;

using System;

/// <summary>
/// Interface of the dependency graph of a service collection.
/// </summary>
public interface IDependencyGraph : IReadOnlyDependencyGraph
{
    /// <summary>
    /// Adds the given root node to the graph.
    /// </summary>
    /// <param name="root">The root node.</param>
    /// <exception cref="ArgumentNullException"><paramref name="root"/> is null.</exception>
    void AddRoot(DependencyNode root);

    /// <summary>
    /// Removes the first root node that matches the given node from the graph.
    /// </summary>
    /// <param name="root">The root node.</param>
    /// <returns>True if the node was removed successfully, otherwise false (if the graph did not contain such a root node).</returns>
    /// <exception cref="ArgumentNullException"><paramref name="root"/> is null.</exception>
    bool RemoveRoot(DependencyNode root);

    /// <summary>
    /// Adds the given dependency to the graph.
    /// </summary>
    /// <param name="service">The service to which the dependency is added.</param>
    /// <param name="dependency">The dependency.</param>
    /// <exception cref="ArgumentNullException"><paramref name="service"/> or <paramref name="dependency"/> is null.</exception>
    void AddDependency(object service, DependencyNode dependency);

    /// <summary>
    /// Removes the first dependency that matches the given dependency from the graph.
    /// </summary>
    /// <param name="service">The service from which the dependency is removed.</param>
    /// <param name="dependency">The dependency.</param>
    /// <returns>True if the dependency was removed successfully, otherwise false (if the graph did not contain such a dependency).</returns>
    /// <exception cref="ArgumentNullException"><paramref name="service"/> or <paramref name="dependency"/> is null.</exception>
    bool RemoveDependency(object service, DependencyNode dependency);
}
