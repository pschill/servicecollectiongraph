﻿namespace ServiceCollectionGraph.Graph;

using ServiceCollectionGraph.DependencyInjection;
using System;
using System.Collections.Generic;

/// <summary>
/// This visitor adds the dependencies within a service collection to a <see cref="IDependencyGraph"/>.
/// </summary>
public class DependencyGraphDependencyVisitor : IDependencyVisitor
{
    private readonly IDependencyGraph _graph;
    private readonly Stack<Item> _stack = new();

    /// <summary>
    /// Constructs the visitor.
    /// </summary>
    /// <param name="graph">The graph to which the dependencies are added.</param>
    /// <exception cref="ArgumentNullException"><paramref name="graph"/> is null.</exception>
    public DependencyGraphDependencyVisitor(IDependencyGraph graph)
    {
        _graph = graph ?? throw new ArgumentNullException(nameof(graph));
    }

    /// <inheritdoc/>
    public void BeginResolveService(Type serviceType)
    {
        ArgumentNullException.ThrowIfNull(serviceType);
        _stack.Push(new Item(serviceType));
    }

    /// <inheritdoc/>
    public void EndResolveService(object? service)
    {
        var item = _stack.Pop();
        if (service is not null)
        {
            foreach (var dependency in item.Dependencies)
            {
                if (service != dependency.Service)
                {
                    _graph.AddDependency(service, dependency);
                }
            }
            var node = new DependencyNode(item.ServiceType, service);
            if (_stack.TryPeek(out var parentItem))
            {
                parentItem.Dependencies.Add(node);
            }
            else
            {
                _graph.AddRoot(node);
            }
        }
    }

    private class Item
    {
        public Item(Type serviceType)
        {
            ServiceType = serviceType ?? throw new ArgumentNullException(nameof(serviceType));
        }

        public Type ServiceType { get; }
        public List<DependencyNode> Dependencies { get; } = new();
    }
}
