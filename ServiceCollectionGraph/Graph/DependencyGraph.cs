﻿namespace ServiceCollectionGraph.Graph;

using System;
using System.Collections.Generic;
using System.Linq;

/// <inheritdoc/>
public class DependencyGraph : IDependencyGraph
{
    private readonly List<DependencyNode> _roots = new();
    private readonly Dictionary<object, List<DependencyNode>> _dependencies = new();

    /// <inheritdoc/>
    public IEnumerable<DependencyNode> GetRoots()
    {
        return _roots;
    }

    /// <inheritdoc/>
    public IEnumerable<DependencyNode> GetDependencies(object service)
    {
        ArgumentNullException.ThrowIfNull(service);
        if (_dependencies.TryGetValue(service, out var serviceDependencies))
        {
            return serviceDependencies;
        }
        return Enumerable.Empty<DependencyNode>();
    }

    /// <inheritdoc/>
    public void AddRoot(DependencyNode root)
    {
        ArgumentNullException.ThrowIfNull(root);
        _roots.Add(root);
    }

    /// <inheritdoc/>
    public bool RemoveRoot(DependencyNode root)
    {
        ArgumentNullException.ThrowIfNull(root);
        return _roots.Remove(root);
    }

    /// <inheritdoc/>
    public void AddDependency(object service, DependencyNode dependency)
    {
        ArgumentNullException.ThrowIfNull(service);
        ArgumentNullException.ThrowIfNull(dependency);
        if (!_dependencies.TryGetValue(service, out var serviceDependencies))
        {
            serviceDependencies = new();
            _dependencies.Add(service, serviceDependencies);
        }
        serviceDependencies.Add(dependency);
    }

    /// <inheritdoc/>
    public bool RemoveDependency(object service, DependencyNode dependency)
    {
        ArgumentNullException.ThrowIfNull(service);
        ArgumentNullException.ThrowIfNull(dependency);
        if (_dependencies.TryGetValue(service, out var serviceDependencies))
        {
            return serviceDependencies.Remove(dependency);
        }
        return false;
    }
}
