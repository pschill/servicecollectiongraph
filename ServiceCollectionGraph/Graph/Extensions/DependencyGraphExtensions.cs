﻿namespace ServiceCollectionGraph.Graph.Extensions;

using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Contains extension methods for <see cref="IDependencyGraph"/> and <see cref="IReadOnlyDependencyGraph"/>.
/// </summary>
public static class DependencyGraphExtensions
{
    /// <summary>
    /// Removes the current root from the given graph and adds their (direct) dependencies as new roots.
    /// </summary>
    /// <param name="graph">The graph.</param>
    /// <exception cref="ArgumentNullException"><paramref name="graph"/> is null.</exception>
    public static void CutRoots(this IDependencyGraph graph)
    {
        ArgumentNullException.ThrowIfNull(graph);
        var newRoots = new HashSet<DependencyNode>();
        foreach (var oldRoot in graph.GetRoots().ToList())
        {
            foreach (var newRoot in graph.GetDependencies(oldRoot.Service).ToList())
            {
                graph.RemoveDependency(oldRoot.Service, newRoot);
                if (newRoots.Add(newRoot))
                {
                    graph.AddRoot(newRoot);
                }
            }
            graph.RemoveRoot(oldRoot);
        }
    }
}
