﻿namespace ServiceCollectionGraph.Graph;

using System;

/// <summary>
/// Single node in a dependency graph.
/// </summary>
public record DependencyNode(Type ServiceType, object Service);
